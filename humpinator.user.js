// ==UserScript==
// @name           Humpinator
// @description    Augment my Hump!
// @namespace      com.nfohump.humpinator
// @author         garus
// @icon           https://www.nfohump.com/inc/images/favicon.ico
// @require        https://code.jquery.com/jquery-3.4.1.min.js
// @require        https://gitlab.com/nfohump/humpinator/raw/master/utils.js
// @require        https://gitlab.com/nfohump/humpinator/raw/master/menu.js
// @require        https://gitlab.com/nfohump/humpinator/raw/master/main.js
// @updateURL      https://gitlab.com/nfohump/humpinator/raw/master/humpinator.user.js
// @downloadURL    https://gitlab.com/nfohump/humpinator/raw/master/humpinator.user.js
// @homepageURL    https://nfohump.com/forum/viewtopic.php?t=75547
// @homepage       https://nfohump.com/forum/viewtopic.php?t=75547
// @website        https://nfohump.com/forum/viewtopic.php?t=75547
// @source         https://nfohump.com/forum/viewtopic.php?t=75547
// @supportURL     https://nfohump.com/forum/viewtopic.php?t=75547
// @license        MIT
// @include        https://www.nfohump.com/forum/*
// @resource       customCSS https://gitlab.com/nfohump/humpinator/raw/master/humpinator.css
// @resource       replyFormJS https://gitlab.com/nfohump/humpinator/raw/master/inject_quick_reply.js
// @version        1.0.8
// @grant          GM_addStyle
// @grant          GM_getResourceText
// @run-at         document-end
// ==/UserScript==